package main

import (
	"os"
	"os/exec"
	"strings"
)

const applescript = `set urlToFind to "https://forge.lmig.com/issues/browse/HSASS-1717"

set exitRepeat2 to false
set tabID to missing value

tell application "Google Chrome"
	set {tabList, urlList} to {it, URL} of tabs of windows
	
	repeat with ndx1 from 1 to length of urlList
		set n1 to ndx1
		repeat with ndx2 from 1 to (length of item ndx1 of urlList)
			set tabIndex to ndx2
			if item ndx2 of (item ndx1 of urlList) contains urlToFind then
				set tabID to item ndx2 of (item ndx1 of tabList)
				set exitRepeat2 to true
				exit repeat
			end if
		end repeat
		if exitRepeat2 then exit repeat
	end repeat
	
	if tabID ≠ missing value then
		
		try
			tabID / 0
		on error e
			set AppleScript's text item delimiters to {"window id ", " of application"}
			set winID to text item 2 of e
		end try
		
		set index of window id winID to 1
		raiseWindowOne() of me
		tell front window to set active tab index to tabIndex
		
	else
		
		tell front window to make new tab at after (get active tab) with properties {URL:urlToFind} -- open a new tab after the current tab
		
	end if
	
end tell

------------------------------------------------------------------------------
--» HANDLERS
------------------------------------------------------------------------------
on raiseWindowOne()
	tell application "System Events"
		tell application process "Google Chrome"
			tell window 1
				perform action "AXRaise"
			end tell
		end tell
	end tell
end raiseWindowOne
`

func activate(tabNumber string) {
	exec.Command("chrome-cli", "activate", "-t", tabNumber).Output()

}

func open(tabUrl string) {
	exec.Command("chrome-cli", "open", tabUrl).Output()

}

func main() {
	res, _ := exec.Command("chrome-cli", "list", "links").Output()
	targetUrl := os.Args[1]
	targetLength := len(targetUrl)
	lines := strings.Split(string(res), "\n")
	for _, line := range lines {

		parts := strings.Split(line, " ")

		if len(parts) > 1 && len(parts[1]) >= targetLength {

			if strings.Compare(strings.Trim(parts[1], " ")[:len(targetUrl)], targetUrl) == 0 {
				tabNumber := strings.Trim(parts[0], "[]")
				activate(tabNumber)
				os.Exit(0)

			}
		}

	}
	open(targetUrl)

}
