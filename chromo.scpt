#!/usr/bin/osascript
on run argv
    set urlToFind to argv

    set exitRepeat2 to false
    set tabID to missing value
    tell application "Google Chrome"
        set {tabList, urlList} to {it, URL} of tabs of windows
        repeat with ndx1 from 1 to length of urlList
            set n1 to ndx1
            repeat with ndx2 from 1 to (length of item ndx1 of urlList)
                set tabIndex to ndx2
                if item ndx2 of (item ndx1 of urlList) contains urlToFind then
                    set tabID to item ndx2 of (item ndx1 of tabList)
                    set exitRepeat2 to true
                    exit repeat
                end if
            end repeat
            if exitRepeat2 then exit repeat
        end repeat
        
        if tabID ≠ missing value then
            
            try
                tabID / 0
            on error e
                set AppleScript's text item delimiters to {"window id ", " of application"}
                set winID to text item 2 of e
            end try
            
            set index of window id winID to 1
            activate
            tell front window to set active tab index to tabIndex
            
        else
            
            tell front window to make new tab at after (get active tab) with properties {URL:urlToFind} -- open a new tab after the current tab
            activate
        end if
        
    end tell
end run


