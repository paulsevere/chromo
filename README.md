# Chromo

Applescript that activates or opens a chrome tab for a given URL

## Download:

```bash
curl  https://gitlab.com/paulsevere/chromo/-/raw/master/chromo.scpt --output  /usr/local/bin/chromo ; chmod u+x /usr/local/bin/chromo
```

## Usage:

```bash
chromo https://www.nationalgeographic.com/
```
